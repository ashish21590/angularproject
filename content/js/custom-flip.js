//script to resize flip slider for small screen
/*function checkWidth() {
    if ($(window).width() >= 2000) {
        $('.screen-flip-slider .ui.grid>.row').attr('class','four1 column row gridabc');
    }
    else if ($(window).width() <= 768) {
        $('.screen-flip-slider .ui.grid>.row').attr('class','three column row gridabc');
    }
    else if ($(window).width() <= 568) {
           $('.screen-flip-slider').hide();
           $('.mobile-flip-slider').show();
           $('.mobile-flip-slider .ui.grid>.row').attr('class','two column row gridabc');   
window.onresize = checkWidth;
window.onload = checkWidth;

 //$('.shape').shape();
$(document).ready(function() { 
function myflip() {
  $(this).shape('flip over').delay(100);
}
});
  // for individual flips
  var i = 0,
      maxi = 8,// TOTAL NUMBER OF IMAGES
      timeperflip = 2000; // time PER FLIP
      setInterval(function(){
        $('.shape').each(function(i, idx){
          x(this,i*timeperflip)
        });
        
        
    i++;
    if(i==maxi){ i=0;}
    console.log(i)
    $('.progress-buttons .active').removeClass('active')
    $('.progress-buttons .button:eq('+i+')').addClass('active')
  },(maxi) * timeperflip)     
  function x(el, time){
      setTimeout(function(){
      $(el).shape('flip over')
    },time)
  }
  // for complete slide transitions
 $('.progress-buttons .button').click(function(){
     var i = $(this).index();
     // alert(i)
     // $('.slider').show();
     // $('.slider').shape('set next side', '.sides .side:eq('+i+')')
     $('.sides .side.active').removeClass('active');
     $('.sides .side:nth-child('+(i+1)+')').addClass('active');
     $(this).addClass('active').siblings().removeClass('active');
     // $('.sides .side.active').removeClass('active');
     $('.slider').transition('flip over');
   });*/

   //$('.shape').shape();

function myflip() {
  // show loading image
  $('#loader_img1').show();

  // main image loaded ?
  $('.body').on('load', function(){
    // hide/remove the loading image
    $('#loader_img1').hide();
    $(this).shape('flip over').delay(100);
  });
  //$(this).shape('flip over').delay(100);
}

  var $container = jQuery('#masonry-grid');
  // initialize
  var $grid = $container.masonry({
    //columnWidth: 400,
     // gutter: 0,
     percentPosition: true,
    itemSelector: '.grid-item'
  });
var masonary = function(){
   $('#masonry-grid').masonry({
  // options
  itemSelector: '.grid-item',
   percentPosition: true
  //columnWidth: 200
 });

}
window.onload = function(){
  $('.main-slider .ui.grid').removeClass('invisible');
  $('.main-slider').removeClass('loading-ico');
    controlFlip() ;
}
window.onresize = function(){
  $('.main-slider .ui.grid').removeClass('invisible');
  $('.main-slider').removeClass('loading-ico');
    controlFlip() ;
}
function controlFlip(){
  //alert('s')
  // for individual flips
  var maxi = $('.main-slider .side.active:visible,.mobile-flip-slider .side.active:visible').length;
  timeperflip = 8000;
  flip = function(i){
    $('.shape:visible:eq('+i+')').shape({
      onChange:function(){
        if (++i == maxi) i =0;
        
        flip(i);
      },
      duration:timeperflip/maxi
    }).shape('flip over');
  }
  flip(0);
}

$(document).ready(function() { 

  // for complete slide transitions
 $('.progress-buttons .button').click(function(){
     var i = $(this).index();
     // alert(i)
     // $('.slider').show();
     // $('.slider').shape('set next side', '.sides .side:eq('+i+')')
     $('.sides .side.active').removeClass('active');
     $('.sides .side:nth-child('+(i+1)+')').addClass('active');
   $(this).addClass('active').siblings().removeClass('active');
     // $('.sides .side.active').removeClass('active');
     // // $('.slider').transition('flip over');
   });
  masonary();
});



