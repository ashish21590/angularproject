//config.js file

require.js.config({
	baseUrl: 'content/js',
	paths: {
		bootstarp: 'bootstrap.min',
		mycustom: 'mycustom',
		jquery: 'jquery-1.9.1.min',
		masonary: 'masonry.pkgd.min',
		customflip: 'custom-flip',
		flatpicker: ['flatpickr','flatpickr.min'],
		imagesloaded: 'imagesloaded',
		jaliswall: 'jaliswall',
		semantic: 'semantic.min',
		timeline: 'timeline',
		hover: 'hover'

	}
});

