

function init() {
        window.addEventListener('scroll', function(e){
            var distanceY = window.pageYOffset || document.documentElement.scrollTop,
                shrinkOn = 300,
                nav = document.querySelector("nav");
            if (distanceY > shrinkOn) {
               // classie.add(nav,"smaller");
            } else {
                if (classie.has(nav,"smaller")) {
                   // classie.remove(nav,"smaller");
                }
            }
        });
    }
    window.onload = init();
/* ends header onscroll script */
/*haeder burger menu and popup*/
$(document).ready(function(){
  $('.popup-btn').click(function(){
    $(this).toggleClass('open');
      $("body").append(''); $(".popup").toggle("medium"); 
      $(".overlay").toggle("medium");
      $( "body" ).toggleClass( "posfixed" );
      
  });
}); 

/*  header script to open searchbar */

$(document).ready(function(){
      $(".search-icon").click(function(){
          $(".dropdown-search").slideToggle();
      });
  });
/* ends header script to open searchbar */

/*popup script of header sign in via */
$(document).ready(function() {       
  $(".signin-popup-btn").click(function(e) {
      var effect = 'slide';
      var options = { direction: $('.mySelect').val() };
      var duration = 500;
      $("body").append(''); $(".signin-popup").show("medium"); 
      $(".overlay").show("medium");
      $(".close").click(function(e) { 
      $(".signin-popup, .overlay").hide("medium"); 
      }); 
  }); 
});
/*popup script of header sign in via ends */

/* script to show hide more sociual icons */
$(document).ready(function(){
      $(".show-more").click(function(){
          $(".hidden-more-icons").slideToggle( "slow" );
          $(".show-more").hide();
      });
  });

/* script to show hide more sociual icons ends  */

/* script to show hide hover cat icon */
$(function() {
    $('.cat-zoom-icon').click(function() {
        $(this).hide();
        $(this).next('.cat-plus-icon').show();
       //$('.cat-plus-icon').show();
    });
    $('.cat-plus-icon').click(function() {
        $(this).hide();
        $(this).prev(".cat-zoom-icon").show();
        //$('.cat-zoom-icon').show();
    });
    $('.cat-minus-icon').click(function() {
        $(this).hide();
        $(this).next('.cat-plus-icon').show();
       //$('.cat-plus-icon').show();
    });
});
/* script to show hide hover cat icon ends */
/* script to show hide hover cat follow brand button */
/*$(function() {
    $('.follow').click(function() {
        $(this).hide();
        $(this).next(".following").show();
        //$(this).parent().parent().parent().parent('.following').css('background','inherit');
        //alert($(this).parent().parent().parent().parent();
        //console.log($(this).parent().parent().parent().parent());
        //$('.following').show();
    });
    $('.following').click(function() {
        $(this).hide();
        //alert($(this).parent().parent().parent().parent());
        $(this).prev(".follow").show();
        //$('.unfollow').show();
    });
    $('.following').hover(function() {
              $('.unfollow').show();
              $('.following').hide();
    });
    $(".unfollow").on('mouseleave',function(){
              $('.unfollow').hide();
              $('.following').show();
    });
    $('.unfollow').click(function() {
        $(this).hide();
        $(this).parent().prev(".follow").show();

    });
    
});*/

/* script to show hide hover cat follow button ends */
//script to hide show the follow status of h1cat follow category
$(function() {
    $(".following-cat").hide();
    $(".unfollow-cat").hide();
    $('.follow-cat').click(function() {
        $(this).hide();
        $(this).next(".following-cat").show();
    });
    $('.following-cat').click(function() {
        $(this).hide();
        $(this).prev(".follow-cat").show();
    });
    $('.following-cat').hover(function() {
              $('.unfollow-cat').show();
              $('.following-cat').hide();
    });
    $(".unfollow-cat").on('mouseleave',function(){
              $('.unfollow-cat').hide();
              $('.following-cat').show();
    });
    
});
//script to hide show the follow status of h1cat ends


/* script to scroll to top icon */
$(document).ready(function () {

    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('.scrollup').fadeIn();
        } else {
            $('.scrollup').fadeOut();
        }
    });

    $('.scrollup').click(function () {
        $("html, body").animate({
            scrollTop: 0
        }, 600);
        return false;
    });

});
/* script to scroll to top icon ends */
/* script to hide show subcat icons on cat page */
$(document).ready(function(){
      $(".sub-cat-btn").click(function(){
          $(".hidden-cat-optns").slideToggle( "slow" );
      });
});
/* script to hide show subcat icons on cat page ends */
/* category page follow btn script */
$(document).ready(function(){
$(".category_pg .normal").hover(
  function () {
    $(this).addClass("cat_fashion");
  },
  function () {
    $(this).removeClass("cat_fashion");

  });

$('.category_pg .following').hover(
      function(){ $(this).addClass('unfollow') },
     function(){ $(this).removeClass('unfollow') }
);

});

$(document).ready(function () {
    $(".category_pg .following").mouseover(function () {
      $('.category_pg .following').text("UNFOLLOW");
    });
    $(".category_pg .following").mouseout(function () {
      $('.category_pg .following').text("FOLLOWING");
    });
  });

$(function() {
    $('.category_pg .normal').click(function() {
        $('.normal').hide();
        $('.following').show();
    });
    $('.category_pg .following').click(function() {
        $('.following').hide();
        $('.normal').show();
    });
    
});
/* category page follow btn script ends */

/* placeholder in category page */
$(document).ready(function(){ 
  $('.textarea1').find("input[type=textarea], textarea").each(function(ev)
  {
      if(!$(this).val()) { 
     $(this).attr("placeholder", "Type your answer here");
  }
  });
});
/* placeholder in category page ends */
$(document).ready(function(){
      $('.fwd-icon-sectn').click(function(){
          //alert("hello");
          var dis= $(this).parent().parent().parent().parent().children('.fwd-social-icons').is(':visible');
          $(this).addClass( "active" );
          //alert(dis);
          if(dis==false){
           // alert("ok");
              $(this).parent().parent().parent().parent().children('.fwd-social-icons').slideDown();
            }
            else{
              ///alert("not ok");
              $(this).parent().parent().parent().parent().children('.fwd-social-icons').slideUp();
            }
      });
});

$(document).ready(function(){
      $('.comment-sectn').click(function(){
          //alert("hello");
          var dis= $(this).parent().parent().parent().parent().children('.write-comment').is(':visible');
          //alert(dis);
          if(dis==false){
           // alert("ok");
              $(this).parent().parent().parent().parent().children('.write-comment').slideDown();
            }
            else{
              ///alert("not ok");
              $(this).parent().parent().parent().parent().children('.write-comment').slideUp();
            }
      });
});
/* profile page details hide on scroll */
/*$(window).scroll(function() {
    if ($(this).scrollTop()>0)
     {
        $('.onscroll-hide-con').hide("slow");
     }
    else
     {
      $('.onscroll-hide-con').show("slow");
     }
 });*/
/*ends profile page details hide on scroll */          
/* script to hide show try now form */
 /*$(document).ready(function(){
    $(".test-drive-btn").click(function(){
        $(".try-now-con").slideToggle( "slow" );
        $('html,body').animate({
        scrollTop: $(".content_headline-categorypg").offset().top-100},
        'slow');
    });
 });*/
$(document).ready(function() {
    $(".test-drive-btn").on("click", function( e ) {
        e.preventDefault();
        $(".try-now-con").slideDown();
        $("body, html").animate({ 
            scrollTop: $( $(this).attr('.content_headline-categorypg') ).offset().top 
        }, 600);

    });

    $(".close-btn").on("click", function( e ) {
        $(this).parent(".try-now-con").slideUp("slow");
    });
});

  $(document).ready(function(){
    $(".try-now-tab").click(function(){
        $(".try-now-con").slideDown( "slow" );
    });
 });
$(document).ready(function(){
   $("#btn").click(function(){
       $(".sendquery-form").slideDown( "slow" );
      
       $('html,body').animate({
       scrollTop: $(".btn-sectn").offset().top+100},
       'slow');
   });
   $(".close-btn").on("click", function( e ) {
        $(this).parent(".sendquery-form").slideUp("slow");
    });
});
/* ends script to hide show try now form */


/*script for product page slider expand icon width switch */
$(document).ready(function(){
    $(".expand-slide-img").click(function(){
        $(".product-carousel-inner").toggleClass("fullwidth");
    });
});
/*  collection script to show privacy */
  $(document).ready(function(){
      $(".lock").click(function(){
          $(".privacy-status").slideToggle( "slow" );
          $(".fa-unlock-alt").toggle();
          $(".fa-lock").toggle();
      });
  });
 //collection edit overlay trigger 
 $(document).ready(function(){
    $(".edit-btn").click(function(){
        $(".edit-overlay").slideToggle( "medium" );
    });
}); 
//script to toggle icon in specification tab
$(document).ready(function(){ 
  $('.spec-headline').click(function(){
      $(this).find('i').toggleClass('fa-chevron-up');
      $(this).find('i').toggleClass('fa-chevron-down')
  });  
});


/*popup script of header settings */
$(document).ready(function() {       
  $(".settings-popup-btn").click(function(e) {
      var effect = 'slide';
      var options = { direction: $('.mySelect').val() };
      var duration = 500;
      $("body").append(''); $(".settings-popup").show("medium"); 
      $(".overlay").show("medium");
      $(".close").click(function(e) { 
      $(".settings-popup, .overlay").hide("medium"); 
      }); 
  }); 
});
/*popup script of header settings ends */
// script for the settings page
/*$(function() {
    $('.settings-blocks .edit').click(function() {
      $(this).parent().children("div").slideToggle("slow");
    });
}); */
 $(function () {
     $(".settings-blocks .edit").click(function (e) {
      e.preventDefault();
      $(this).parent().children("div").not($(this).toggleClass('open').next('div').slideToggle("slow")).slideUp('fast');
      $(this).parent().children("div").not(target).hide();
  
     });
});

// script to show password on sign up page

// script for the adding new email address on settings page
$(function() {
    $('.add-email-click,.add-ph-click').click(function() {
       $(this).siblings("div").slideToggle("slow"); 
    });
}); 

//script for the timeline comment popup
$(document).ready(function() {       
  $(".tag").click(function(e) {
      var effect = 'slide';
      var options = { direction: $('.mySelect').val() };
      var duration = 500;
      $("body").append(''); $(".comment-pop").show("medium","linear",function(){ 
         $("#comment-post1").mCustomScrollbar({
                autoHideScrollbar:true,
                theme:"light-thin"
            });
      });
      $(".overlay").show("medium");
      $(".close").click(function(e) { 
      $(".comment-pop,.overlay").hide("medium","linear",function(){ 
         $(".mCSB_scrollTools").css({"display": "none"});
      }); 

      }); 
    $( "#header_nav" ).addClass( "high-z" );   
  }); 
});  



//script for the timeline comment img popup
$(document).ready(function() {       
  $(".popimg").click(function(e) {
      var effect = 'slide';
      var options = { direction: $('.mySelect').val() };
      var duration = 500;
      $("body").append(''); $(".img-post").show("medium","linear",function(){ 
         $("#content_1").mCustomScrollbar({
                autoHideScrollbar:true,
                theme:"light-thin"
            });
      });
      $(".overlay").show("medium");
      $(".close").click(function(e) { 
      $(".img-post, .overlay").hide("medium","linear",function(){ 
         $(".mCSB_scrollTools").css({"display": "none"});
      }); 
      }); 
  }); 
});   


var onsload = function(){

$('.follow-brand-btn').click( function(e){
  if( $(this).hasClass('follow')) follow(e, this);
  else if( $(this).hasClass('following')) unfollow(e, this);
})

var follow = function (event, mythis) {
  event.stopPropagation();
  event.preventDefault();
  $(mythis).removeClass('follow').addClass('following');
    $("#alert-msg").text(" You have chosen to follow the brand"); $(".alert-sectn").fadeIn();
    setTimeout(function () {
      $('.alert-sectn').fadeOut();
    }, 3000);
};

//for unfollowing the brand
var unfollow = function (event, mythis) {
  event.stopPropagation();
  $(mythis).removeClass('following').addClass('follow')

}


}// end of onload



$(document).ready(function () {
    $('.follow-cat-btn').click( function(e){
        if( $(this).hasClass('follow-cat')) followcat(e, this);
        else if( $(this).hasClass('following-cat')) unfollowcat(e, this);
    })
});
// for the following category
var followcat = function (event, mythis) {
    //alert("category click");
    var user_id = $("#myid").html();
    $(mythis).removeClass('follow-cat');
    $(mythis).addClass('following-cat');
    $("#alert-msg").text(" You have chosen to follow the category"); $(".alert-sectn").fadeIn();
    setTimeout(function () {
      $('.alert-sectn').fadeOut();
    }, 3000);
    
};
var unfollowcat = function (event, mythis) {
    var user_id = $("#myid").html();
    $(mythis).removeClass('following-cat').addClass('follow-cat');
}

//profile timeline page gallery images magnific popup script
/*$(document).ready(function () {
   $('.image-link').magnificPopup({ type: 'image' });
});
$('#some-button').magnificPopup({
   items: {
       src: 'Content/images/01_Pre-Launch_06.png'
   },
   type: 'image' // this is default type
});*/

$('.request-href').click(function() {
    $('.current').removeClass('current').hide()
        .next().show().addClass('current');
});

$('.go-back-href').click(function() {
    $('.current').removeClass('current').hide()
        .prev().show().addClass('current');
});

// functionality for follow curator category
$(document).ready(function () {
    $('.follow-curator-btn,.follow-curator-btn1').click( function(e){
        if( $(this).hasClass('follow1cur')) followcur(e, this);
        else if( $(this).hasClass('following1cur')) unfollowcur(e, this);
    })
});
// for the following category
var followcur = function (event, mythis) {
    //alert("category click");
    var user_id = $("#myid").html();
    $(mythis).removeClass('follow1cur');
    $(mythis).addClass('following1cur');
    $("#alert-msg").text(" You have chosen to follow the category"); $(".alert-sectn").fadeIn();
    setTimeout(function () {
      $('.alert-sectn').fadeOut();
    }, 3000);
    
};
var unfollowcur = function (event, mythis) {
    var user_id = $("#myid").html();
    $(mythis).removeClass('following1cur').addClass('follow1cur');
}

//follow curator page
$(document).ready(function () {
   $('.follow-curator-btn').click( function(e){
       if( $(this).hasClass('follow1cur')) followcur(e, this);
       else if( $(this).hasClass('following1cur')) unfollowcur(e, this);
   })
});
// for the following category
var followcur = function (event, mythis) {
   //alert("category click");
   var user_id = $("#myid").html();
   $(mythis).removeClass('follow1cur');
   $(mythis).addClass('following1cur');
   $("#alert-msg").text(" You have chosen to follow the category"); $(".alert-sectn").fadeIn();
   setTimeout(function () {
     $('.alert-sectn').fadeOut();
   }, 3000);
   
};
var unfollowcur = function (event, mythis) {
   var user_id = $("#myid").html();
   $(mythis).removeClass('following1cur').addClass('follow1cur');
}
// functionality for follow nd unfollow curator segement
$(document).ready(function () {
   $('.follow-seg').click( function(e){
       if( $(this).hasClass('follow1seg')) followseg(e, this);
       else if( $(this).hasClass('following1seg')) unfollowseg(e, this);
   })
});
var followseg = function (event, mythis) {
   //alert("category click");
   var user_id = $("#myid").html();
   $(mythis).removeClass('follow1seg');
   $(mythis).addClass('following1seg');
   $("#alert-msg").text(" You have chosen to follow the segement"); $(".alert-sectn").fadeIn();
   setTimeout(function () {
     $('.alert-sectn').fadeOut();
   }, 3000);
   
};
var unfollowseg = function (event, mythis) {
   var user_id = $("#myid").html();
   $(mythis).removeClass('following1seg').addClass('follow1seg');
}

