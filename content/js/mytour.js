
$(document).ready(function () {
  alert("m working");
  console.log("tour is working");
  // Instance the tour
var tour = new Tour({
  steps: [
  {
    element: ".popup-btn",
    title: "This is main menu",
    content: "This consist of main menu header part"
  },
  {
    element: ".crown-sectn",
    title: "crown",
    content: "This is to like the post and product"
  },
  {
    element: ".news-cat",
    title: "news",
    content: "This is to identify the category as news"
  },
  {
    element: ".follow-cat-btn",
    title: "follow category button",
    content: "This is to follow the category"
  },
  {
    element: ".follow-brand-btn",
    title: "follow brand button",
    content: "This is to follow the brand"
  },
  {
    element: ".cat-zoom-icon",
    title: "collection icon",
    content: "This is to add the product to your collection"
  },
  {
    element: ".signin-popup-btn",
    title: "sign in button",
    content: "from here you can sign in"
  }
],
container: "body",
  smartPlacement: true,
  keyboard: true,
  storage: window.localStorage,
  debug: false,
  backdrop: true,
  backdropContainer: 'body',
  backdropPadding: 0,
  redirect: true,
  orphan: false,
  duration: false,
  delay: false,
  basePath: ""
});

// Initialize the tour
tour.init();

// Start the tour
tour.start();
    
});